using UnityEngine;
using System.Collections.Generic;

namespace trashgame
{
    public class TrashGenerator:MonoBehaviour
    {
        [SerializeField]
        private List<GameObject> prefabs;
        [SerializeField]
        private int minCount = 1;
        [SerializeField]
        private int maxCount = 1;
        [SerializeField]
        private float areaWidth = 1f;
        [SerializeField]
        private float areaHeight = 1f;

        private void Start()
        {
            int count = Random.Range(minCount, maxCount + 1);
            for(int i = 0; i < count; i++)
            {
                createTrash();
            }
        }

        private void createTrash()
        {
            int prefabId = Random.Range(0, prefabs.Count);
            GameObject prefab = prefabs[prefabId];
            Vector3 pos = getRandomPosInArea(areaWidth, areaHeight);
            pos.y = 1.5f;
            GameObject trash = GameObject.Instantiate(prefab, pos, Quaternion.identity);
            trash.transform.parent = transform;
        }

        private Vector3 getRandomPosInArea(float width, float height)
        {
            float halfAreaW = width / 2f;
            float halfAreaH = height / 2f;

            Vector3 pos = new Vector3();
            pos.x = transform.position.x + Random.Range(-halfAreaW, halfAreaW);
            pos.z = transform.position.z + Random.Range(-halfAreaH, halfAreaH);
            return pos;
        }

        private void OnDrawGizmosSelected()
        {
            Color baseColor = Gizmos.color;

            Gizmos.color = Color.red;
            Gizmos.DrawWireCube(transform.position, new Vector3(areaWidth, 0f, areaHeight));

            Gizmos.color = baseColor;
        }
    }
}